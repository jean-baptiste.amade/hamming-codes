//
//  main.cpp
//  hamming
//
//  Created by Jb on 16/01/2024.
//

#include <iostream>
#include <vector>
#include <bitset>
#include "BooleanMatrix.hpp"
#include "BooleanWord.hpp"

int main(int argc, const char * argv[]) {
    
    // Parameters to change are here
    int p = 3;
    std::string wordToCodeStr = "1011";
    int flipIndex = 1;
    
    // Do not change anything else
    int n = pow(2, p) - 1;
    int k = pow(2, p) - p - 1;
    
    std::cout << "Word to code : " << wordToCodeStr << std::endl;
    
    std::cout << "p = " << p << std::endl;
    std::cout << "n = " << n << std::endl;
    std::cout << "k = " << k << std::endl;
    
    BooleanMatrix H = BooleanMatrix(n-k, n);
    
    H.init();
    
    std::cout << "H matrix is : " << std::endl;
    H.print();
    
    BooleanWord word = BooleanWord(wordToCodeStr, n);
    std::cout << "Original Word : ";
    word.print();
    
    BooleanWord codedWord = BooleanWord(&word, H);
    std::cout << "Coded Word : ";
    codedWord.print();
    
    std::cout << "Is coded word valid ? " << (codedWord.checkValidty() ? "Yes" : "No") << std::endl;
    
    std::cout << "Flipping index " << flipIndex << std::endl;
    codedWord.flipbit(flipIndex);
    codedWord.print();
    
    std::cout << "Is coded word still valid ? " << (codedWord.checkValidty() ? "Yes" : "No") << std::endl;
    
    codedWord.correct();
    
    codedWord.print();
    
    return 0;
}
