//
//  BooleanMatrix.hpp
//  hamming
//
//  Created by Jb on 16/01/2024.
//

#ifndef Matrix_hpp
#define Matrix_hpp

#include <stdio.h>
#include <iostream>
#include <math.h>

struct Dimensions{
    int rows;
    int columns;
};

class BooleanMatrix{
public:
    BooleanMatrix(int rows, int columns);
    void init();
    void print();
    bool read(int level, int index);
    Dimensions getDimensions();
    
private:
    Dimensions dimensions;
    bool** array;
};

#endif /* Matrix_hpp */
