//
//  BooleanMatrix.cpp
//  hamming
//
//  Created by Jb on 16/01/2024.
//

#include "BooleanMatrix.hpp"

BooleanMatrix::BooleanMatrix(int _rows, int _columns){
    BooleanMatrix::dimensions.rows = _rows;
    BooleanMatrix::dimensions.columns = _columns;
    BooleanMatrix::array = new bool*[_rows];
    for (int i = 0; i < _rows; ++i) {
        array[i] = new bool[_columns];
    }
}

void BooleanMatrix::init(){
    for (int i = 0; i < BooleanMatrix::dimensions.rows; ++i){
        int counter = 0;
        bool currentBool = true;
        for (int j = BooleanMatrix::dimensions.columns-1; j >= 0; --j){
            if (counter == pow(2, BooleanMatrix::dimensions.rows - i - 1)){
                currentBool = !currentBool;
                counter = 0;
            }
            BooleanMatrix::array[i][j] = currentBool;
            counter++;
        }
    }
}

void BooleanMatrix::print(){
    for (int i = 0; i < BooleanMatrix::dimensions.rows; ++i) {
        for (int j = 0; j < BooleanMatrix::dimensions.columns; ++j) {
            std::cout << BooleanMatrix::array[i][j] << ' ';
        }
        std::cout << '\n';
    }
}

bool BooleanMatrix::read(int level, int index){
    return BooleanMatrix::array[BooleanMatrix::dimensions.rows-level-1][index];
}

Dimensions BooleanMatrix::getDimensions(){
    return Dimensions{BooleanMatrix::dimensions.rows, BooleanMatrix::dimensions.columns};
}
