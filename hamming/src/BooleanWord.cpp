//
//  BooleanWord.cpp
//  hamming
//
//  Created by Jb on 16/01/2024.
//

#include "BooleanWord.hpp"

BooleanWord::BooleanWord(std::string _word, int _n) : matrix(matrix){
    BooleanWord::size = (int)_word.length();
    BooleanWord::n = _n;
    BooleanWord::wordArray = new bool[BooleanWord::size];
    
    // convert the string to a boolean array
    for (int i = 0; i < BooleanWord::size; ++i){
        BooleanWord::wordArray[i] = _word[i]=='1' ? true : false; // ternary operators forever
    }
}

void BooleanWord::print(){
    for (int i = 0; i < BooleanWord::size; ++i){
        std::cout << *(wordArray+i) << ' ';
    }
    std::cout << std::endl;
}


bool BooleanWord::getParity(int level){
    // This method should only be run on pre-coded words
    
    int counter = 0;
    for (int i = 0; i < BooleanWord::size ; ++i){
        if (BooleanWord::wordArray[i] && BooleanWord::matrix.read(level, i))
            counter++;
    }
    return counter % 2 == 1 ? true : false;
}

BooleanWord::BooleanWord(BooleanWord *parent, const BooleanMatrix& _matrix) : matrix(matrix){
    BooleanWord::size = parent->n;
    BooleanWord::matrix = _matrix;
    BooleanWord::wordArray = new bool[BooleanWord::size];
    
    // write 1 where the parity bits will stand
    for (int i = 0 ; i < BooleanWord::size ; ++i){
        if ((i & (i + 1)) == 0){
            BooleanWord::wordArray[i] = true;
        }
    }
    
    
    // copy the original word while jumping over parity bits
    int offset = 0;
    for (int i = 0; i < parent-> size; ++i){
        while (BooleanWord::wordArray[i + offset])
            offset++;
        BooleanWord::wordArray[i+offset] = parent->wordArray[i];
    }
    
    // set the correct value of the parity flags
    for (int i = 0 ; i < BooleanWord::size ; ++i){
        if ((i & (i + 1)) == 0){
            BooleanWord::wordArray[i] = false;
            double power = log2(i+1);
            int level = static_cast<int>(power);
            BooleanWord::wordArray[i] = BooleanWord::getParity(level);
        }
    }
    
}

void BooleanWord::flipbit(int index){
    if (index <= 0 || index > BooleanWord::size)
        std::cerr << "[-] Index " << index << " is not in range [0, " << BooleanWord::size << "]" << std::endl;
    BooleanWord::wordArray[index-1] = !BooleanWord::wordArray[index-1];
}

bool BooleanWord::checkValidty(){
    int p = BooleanWord::matrix.getDimensions().rows;
    BooleanWord::syndrome = new bool[p];
    
    for (int i=0; i < p; ++i){
        int sum = 0;
        for (int j = 0; j < BooleanWord::size; ++j){
            sum += (BooleanWord::wordArray[j] & BooleanWord::matrix.read(p-1-i, j));
        }
        BooleanWord::syndrome[i] = sum % 2;
    }
    
    for (int i = 0; i < p; ++i)
        if (BooleanWord::syndrome[i]) return false;
    return true;
}

void BooleanWord::correct(){
    if (BooleanWord::checkValidty()){
        std::cout << "Nothing to correct" << std::endl;
        return;
    }
    
    int p = BooleanWord::matrix.getDimensions().rows;
    std::cout << "Syndrome : ";
    for (int i = 0; i < p; ++i){
        std::cout << BooleanWord::syndrome[i] << ' ';
    }
    std::cout << std::endl;
    
    int index = 0;
    for (int i = 0; i < p; ++i){
        if (BooleanWord::syndrome[i])
            index += (int)pow(2, p-i-1);
    }
    std::cout << "Bit " << index << " was flipped. Flipping it back" << std::endl;
    
    BooleanWord::flipbit(index);
    if (BooleanWord::checkValidty())
        std::cout << "Success correcting the message" << std::endl;
    else
        std::cout << "[-] Message still seems be invalid :/" << std::endl;
}
