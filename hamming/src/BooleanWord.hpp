//
//  BooleanWord.hpp
//  hamming
//
//  Created by Jb on 16/01/2024.
//

#ifndef BooleanWord_hpp
#define BooleanWord_hpp

#include <stdio.h>
#include <iostream>
#include "BooleanMatrix.hpp"

class BooleanWord{
public:
    BooleanWord(std::string word, int n);
    BooleanWord(BooleanWord *parent, const BooleanMatrix& _matrix);
    void print();
    BooleanWord* codedWord;
    void flipbit(int index);
    bool checkValidty();
    void correct();
    
private:
    int size;
    int n;
    bool* wordArray = nullptr;
    bool* syndrome = nullptr;
    BooleanMatrix matrix;
    bool getParity(int level);
};

#endif /* BooleanWord_hpp */
