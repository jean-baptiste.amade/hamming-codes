#  Hamming codes

This is a C++ implementation of Hamming codes (and error correction) as discussed in Alessandro Palumbo's advanced networking course.

## Getting Started

```
git clone https://gitlab-student.centralesupelec.fr/jean-baptiste.amade/hamming-codes.git
cd hamming-codes/hamming
make
bin/hamming
```

If you have a Mac with Xcode installed, you can directly open this project in Xcode. 

## Understanding the code
That is not recommended, nor supported in this version, sorry.
